![CIMPA](course/images/cimpa-2021.png)

# Geometric and numerical methods in optimal control II

We present in this course the **indirect simple shooting** method based on the 
[Pontryagin Maximum Principle (PMP)](https://en.wikipedia.org/wiki/Pontryagin%27s_maximum_principle) to solve a smooth optimal control problem.
By smooth, we mean that the maximization condition of the PMP gives a control law in feedback form (i.e. with respect to the state and the costate)
 at least [continuously differentiable](https://en.wikipedia.org/wiki/Smoothness#Differentiability_classes).
 We use the [nutopy package](https://ct.gitlabpages.inria.fr/nutopy/) to solve the optimal control problem by simple shooting.

When the control law in feedback form is discontinuous, in the indirect context, we need to implement a **multiple** shooting method.
 We will give a brief introduction to the multiple shooting method during the computer session on a simple example.

## Lecture

The indirect simple shooting method

* [Part I](course/lecture/lecture1.md)
* [Part II](course/lecture/lecture2.md)
* [Lecture notebook](course/lecture/lecture_simple_shooting.ipynb)
* [Lecture notes](course/lecture/lecture_simple_shooting.pdf)

To get the lecture, you can either click on the links or follow the procedure given below to download all the course, that is the lecture with the exercices.

The lecture notebook contains the parts I and II together with some numerical resolutions of shooting equations and an exercice to code.

## Exercices

* [Exercise 1: Application of the simple shooting method](course/exercices/ex1_application_simple_shooting.ipynb) 
 ([correction](course/corrections/ex1_cor_application_simple_shooting.ipynb))
* [Exercise 2: Implementing the indirect simple shooting method](course/exercices/ex2_implement_simple_shooting.ipynb)
 ([correction](course/corrections/ex2_cor_implement_simple_shooting.ipynb))
* [Exercise 3: Introduction to indirect multiple shooting: the Bang-Singular-Bang case on a turnpike problem ](course/exercices/ex3_multiple_shooting_bsb.ipynb)
 ([correction](course/corrections/ex3_cor_multiple_shooting_bsb.ipynb))

 To do the exercises, please follow the procedure given below.

[FAQ](https://optimalcontrol.zulip.beta.cimpa-lms.info/#narrow/stream/285-geometric_methods2)

## References

* [Cimpa school](https://cimpa.lis-lab.fr)
* [ct (control toolbox)](https://ct.gitlabpages.inria.fr/gallery)
* [nutopy](https://ct.gitlabpages.inria.fr/nutopy/)
* [Geometric and numerical methods in optimal control I](https://gitlab.polytech.unice.fr/CIMPA/gnmoc)

## Procedure to get the course and do the exercices

**_Remark._** For the students of the CIMPA school, follow the procedure given here: [CIMPA procedure](cimpa_procedure.md).

In the following, `ROOT` is a variable standing for the root directory where the course repository will be stored. For example, 
```bash
export ROOT=${HOME}/cimpa
```

The lecture and exercices are given in the form of python notebooks. To download all the course you can download all the files clicking on the links given above or use the `git` command ([git](https://git-scm.com/downloads)). To clone the git repository and get the course:
```bash
cd $ROOT
git clone https://gitlab.irit.fr/toc/cimpa/gnmoc.git
```

The course uses python packages (see `pkg/env` directory). All theses packages may be installed in a conda environment for ease of use.
If you do not have the `conda` command, please install [miniconda](https://docs.conda.io/en/latest/miniconda.html) or [conda](https://docs.conda.io/en/latest/) (conda is longer to install than miniconda). Then, create the cimpa conda environment:
```bash
cd $ROOT/gnmoc
conda env create -f pkg/env/cimpa-<YOUR_OS>.yaml
```
Where <YOUR_OS> must be replaced by ```linux```, ```mac``` or ```windows```, according to the platform used.

Activate the cimpa conda environment to get access to all the packages:
```bash
conda activate cimpa
```

Run jupyterlab and access to the lecture (in subdirectory `course/lecture`) and exercices (in subdirectory `course/exercices`):
```bash
jupyter-lab
```

## Revoming the environment

You can remove the cimpa environment doing:
```bash
conda activate base
conda env remove --name cimpa
```
