* Go the url https://tljh.polytech.unice.fr/ and connect.

* Click on `New - Terminal`:

![Image New terminal](course/images/new_terminal.png)

* The lecture and exercices are given in the form of python notebooks. To download all the course you can use the `git` command ([git](https://git-scm.com/downloads)). To clone the git repository and get the course, enter the command in the Terminal:

```bash
git clone https://gitlab.irit.fr/toc/cimpa/gnmoc.git
```

![Image Git Clone](course/images/git_clone.png)

* Coming back to your account at https://tljh.polytech.unice.fr/, you should see the directory `gnmoc`:

![Image gnmoc](course/images/gnmoc.png)

* The lecture is in the sub-directory `course/lecture` while the exercices are in `course/exercices`. You can click on any notebook to open it.
