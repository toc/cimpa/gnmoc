# Lecture: The indirect simple shooting method - Part I

* Author: Olivier Cots
* Date: March 2021

------

[Part II](lecture2.md)

------

**_Abstract_**

We present in this notebook the **indirect simple shooting** method based on the [Pontryagin Maximum Principle (PMP)](https://en.wikipedia.org/wiki/Pontryagin%27s_maximum_principle) to solve a smooth optimal control problem. By smooth, we mean that the maximization condition of the PMP gives a control law in feedback form (i.e. with respect to the state and the costate) at least [continuously differentiable](https://en.wikipedia.org/wiki/Smoothness#Differentiability_classes).

We use the [nutopy](https://ct.gitlabpages.inria.fr/nutopy/) package to solve the optimal control problem by simple shooting. You can find another smooth example with more details about the use of nutopy at this [page](https://ct.gitlabpages.inria.fr/gallery/smooth_case/smooth_case.html): note that in this example, the nutopy package is interoperated with the [bocop](https://ct.gitlabpages.inria.fr/bocop3/) software, implementing a direct collocation method.

**_Goal_**

The goal of this presentation is that at the end, you will be able to implement an indirect simple shooting method with nutopy package on an academic optimal control problem for which the optimal control (that is the solution of the problem) is smooth. We assume you have some basic knowledge on optimal control theory. To achieve the goal, you can start by simply read the text and the code at the end of the notebook. For a deeper understanding and more details, you can watch the videos embedded in the notebook. All these videos explain the material you can find here: [pdf file](https://gitlab.inria.fr/ct/gallery/-/raw/master/examples/shooting_tutorials/lecture.pdf).

**_Contents_**

[[_TOC_]]

## I) Statement of the optimal control problem and necessary conditions of optimality

### a) Definition of the optimal control problem

We consider the following smooth (all the data are at least $`C^1`$) *Optimal Control Problem* (OCP) in Lagrange
form, with fixed initial condition and final time:

```math
    \left\{ 
    \begin{array}{l}
        \displaystyle J(u)  := \displaystyle \int_0^{t_f} L(x(t),u(t)) \, \mathrm{d}t \longrightarrow \min \\[1.0em]
        \dot{x}(t) =  f(x(t),u(t)), \quad  u(t) \in U, \quad t \in [0, t_f] \text{ a.e.}, \\[1.0em]
        x(0) = x_0 , \quad c(x(t_f)) = 0_{\mathrm{R}^k},
    \end{array}
    \right. 
```

with $`U \subset \mathrm{R}^m`$ an arbitrary control set and with $`c`$ a smooth application such that its
Jacobian $`c'(x)`$ (or $`J_c(x)`$) is of full rank for any $`x`$ satisfying the constraint $`c(x)=0`$. 
The solution $`u`$ belongs to the *set of control laws* $`L^\infty([0, t_f], \mathrm{R}^m)`$.  

[Additional comments (video)](http://www.youtube.com/watch?v=QUbeyLNZR8A)

### b) Application of the Pontryagin Maximum Principle

Let us denote by

```math
    H(x,p,u) := p \, f(x,u) + p^0\, L(x,u),
```

the *pseudo-Hamiltonian* (that is the non-maximized Hamiltonian) associated to the optimal control problem.

>>>

**_Pontryagin Maximum Principle_**
    
According to the PMP, if $`u`$ is solution of the problem (with $`x`$ the *associated trajectory*),
then there exists a *covector* $`p`$ (which is 
[absolutely continuous](https://en.wikipedia.org/wiki/Absolute_continuity)),
a scalar $`p^0 \in \{-1, 0\}`$, a *Lagrange multiplier* $`\lambda`$, such that: 

1. $`(p, p^0) \ne (0,0)`$,

2. $`\displaystyle \dot{x}(t) = \nabla_p H(x(t),p(t),u(t))`$, 
$`\displaystyle \dot{p}(t) = -\nabla_x H(x(t),p(t),u(t))`$, a.e on $`[0, t_f]`$,

3. $`\displaystyle  H(x(t),p(t),u(t)) = \max_{w \in U} H(x(t), p(t), w)`$ a.e on $`[0, t_f]`$
 (maximization condition),

4. $`\displaystyle p(t_f) = J_c^T(x(t_f)) \lambda = \sum_{i=1}^k \lambda_i \nabla c_i(x(t_f))`$
 (transversality condition).

>>>

>>>
    
**_Assumptions_**
    
We assume the following:
    
* $`U = \mathrm{R}^m`$,
* $`\forall (x,p) \in \mathrm{R}^n \times \mathrm{R}^n`$, $`u \mapsto H(x,p,u)`$ has a unique maximum denoted $`\varphi(x,p)`$ (or $`u[x,p]`$ to recall the fact that it is the control law in feedback form),
* $`\varphi`$ is smooth, that is at least $`C^1`$.  
    
>>>

Under these assumptions, the maximization condition (3) is equivalent to the first order necessary condition of optimality and we have:

```math
    \forall (x,p), \quad \nabla_u H(x,p, \varphi(x,p)) = 0.
```

[Additional comments (video)](http://www.youtube.com/watch?v=xedLNn08Kn4)

### c) The hidden true Hamiltonian

Let us define the Hamiltonian 

```math
h(z) := H(z, \varphi(z)), \quad z=(x,p).
```

This is a true Hamiltonian given by the maximization of the pseudo-Hamiltonian $`H(z,u)`$.  
By the chain rule, we have:

```math
    h'(z) = \frac{\partial H}{\partial z}(z, \varphi(z)) + \frac{\partial H}{\partial u}(z, \varphi(z)) \cdot \varphi'(z) = \frac{\partial H}{\partial z}(z, \varphi(z)),
```

since $`\frac{\partial H}{\partial u}(z, \varphi(z))=0`$.   This leads to the remarkable fact that under our assumptions, equations (2) and (3) are equivalent to the Hamiltonian differential equation

```math
    \dot{z}(t) = \vec{h}(z(t)),
```

where $`\vec{h}(z) := (\nabla_p h(z), -\nabla_x h(z))`$ is the **symplectic gradient** or **Hamiltonian system** associated to $`h`$.  

[Additional comments (video)](http://www.youtube.com/watch?v=inCRJZWT3Ng)

### d) Illustration of the resolution of the necessary conditions of optimality

[Additional comments (video)](http://www.youtube.com/watch?v=oRm8_Grs2Aw)

## II) Examples and boundary value problems

### a) Simple 1D example

**_Remark:_** The interest of this example is to present the methodology to solve the conditions given by the Pontryagin maximum principle.

#### Step 1: Definition of the optimal control problem

We consider the optimal control problem:

```math
    \left\{ 
    \begin{array}{l}
        \displaystyle J(u)  := \displaystyle \frac{1}{2} \int_0^{t_f} u^2(t) \, \mathrm{d}t \longrightarrow \min \\[1.0em]
        \dot{x}(t) =  \displaystyle -x(t)+u(t), \quad  u(t) \in \mathrm{R}, \quad t \in [0, t_f] \text{ a.e.}, \\[1.0em]
        x(0) = x_0 , \quad x(t_f) = x_f,
    \end{array}
    \right. 
```

with $`t_f := 1`$, $`x_0 := -1`$, $`x_f := 0`$ and $`\forall\, t \in[0, t_f]`$, $`x(t) \in \mathrm{R}`$. 

#### Step 2: Application of the Pontryagin maximum principle

The pseudo-Hamiltonian reads

```math
    H(x,p,u) := p \, (-x+u) + p^0\, \frac{1}{2} u^2.
```

The PMP gives

```math
        \left\{ 
            \begin{array}{rcl}
                \dot{x}(t)  &=& \phantom{-}\nabla_p H[t] = -x(t)+u(t),   \\[0.5em]
                \dot{p}(t)  &=& -\nabla_x H[t] = p(t),         \\[0.5em]
                0           &=& \phantom{-}\nabla_u H[t] = p(t)+p^0 u(t),
            \end{array}
        \right.
```

where $`[t] := (x(t),p(t),u(t))`$.  If $`p^0 = 0`$, then $`p = 0`$ by the third equation and so $`(p, p^0) = (0,0)`$ which is not. Hence, any *extremal* $`(x, p, p^0, u)`$ given by the PMP is said to be *normal*, that is $`p^0 = -1`$ (an extremal is said *abnormal* when $`p^0=0`$). 

**_Remark:_** We do not consider the transversality condition when the target $`x_f`$ is fixed. We can retrieve simply the Lagrange multiplier by the relation $`p(t_f)=\lambda`$. 

**_Remark:_** The maximization condition,
$`
H[t] = \max_{w \in \mathrm{R}} H(x(t), p(t), w),
`$
is equivalent here to the condition 
$`
\nabla_u H[t] = 0
`$
 by concavity.

Solving $`\nabla_u H[t] = 0`$, the control satisfies $`u(t) = u[x(t), p(t)] := p(t)`$ where we have introduced the smooth function on $`\mathrm{R} \times \mathrm{R}`$:

```math
u[x,p] = p.
```

**_Remark:_** Plugging the control law in feedback form into the pseudo-Hamiltonian gives the (maximized) Hamiltonian:

```math
    h(z) = H(z, u[z]) = -px + \frac{1}{2} p^2, \quad z = (x, p).
```

#### Step 3: Transcription to a boundary value problem

Now we have the control in feedback form, we introduce the following smooth *Two-Points Boundary Value Problem* (TPBVP or BVP for short):

```math
        \left\{ 
            \begin{array}{rcl}
                \dot{x}(t)  &=& -x(t)+u[x(t),p(t)] = -x(t) + p(t),   \\[0.5em]
                \dot{p}(t)  &=& p(t),         \\[0.5em]
                x(0) &=& x_0, \quad x(t_f) = x_f.
            \end{array}
        \right. 
```

The unknown of this BVP is the initial covector $`p(0)`$.  Indeed, fixing $`p_0:=p(0)`$, then according to the [Cauchy-Lipschitz theorem](https://en.wikipedia.org/wiki/Picard–Lindelöf_theorem), there exists a unique maximal solution denoted 

```math
z(\cdot, x_0, p_0) := (x(\cdot, x_0, p_0), p(\cdot, x_0, p_0))
```

satisfying the dynamics $`\dot{z}(t) = (-x(t)+p(t), p(t))`$ together with the initial condition $`z(0) = (x_0, p_0)`$.  

>>>
**_Goal_**
    
The goal is thus to find the right initial covector $`p_0`$ such that $`x(t_f, x_0, p_0) = x_f`$. 
>>>

#### Step 4: Solving the shooting equation

From $`\dot{p}(t) = p(t)`$, we get
```math
p(t, x_0, p_0) = e^t p_0,
```
which leads to
```math
x(t, x_0, p_0) = p_0 \sinh(t) + x_0 e^{-t}.
```
Solving $`x(t_f, x_0, p_0) = x_f`$, we obtain
```math
 p^*_0 = \frac{x_f - x_0 e^{-t_f}}{\sinh(t_f)} = \frac{2}{e^{2}-1} \approx 0.313.
```

**_Remark:_** To compute $`p^*_0`$, we have solved the linear *shooting equation* 

```math
    S(p_0) := \pi_x( z(t_f, x_0, p_0) ) - x_f = p_0 \sinh(t_f) + x_0 e^{-t_f} - x_f,
```

with $`\pi_x(x,p) := x`$.  Solving $`S(p_0) = 0`$ is what we call the *indirect simple shooting method*.

>>>
**_Summary_**

Note that thanks to the PMP, we have replaced the research of u (which is a function of time) by the research of an element of $`\mathrm{R}`$: the covector $`p_0`$.  The prize of such a drastic reduction is to work in the *cotangent space*, that is the trajectory $`x`$ is lifted in a bigger space and adjoined with a covector $`p`$: this makes the simple shooting method to be qualified of *indirect*. It is important to note that in the indirect methods we work with $`z=(x,p)`$ and not only with the trajectory $`x`$. 
>>>

[Additional comments (video)](http://www.youtube.com/watch?v=r4oYTkF76TA)

### b) Calculus of variations

**_Remark:_** The interest of this part is to present some particular cases in the context of calculus of variations for which our assumptions are satisfied.

[Additional comments (video)](http://www.youtube.com/watch?v=Ud16jTQbsQ0)

### c) An energy min navigation problem 

**_Remark:_** The interest of this example is to present a case where we have some transversality conditions.

We consider the optimal control problem:
```math 
    \left\{ 
    \begin{array}{l}
        \displaystyle J(u)  := \displaystyle \frac{1}{2} \int_0^{t_f} u_1^2(t) + u_2^2(t) \, \mathrm{d}t \longrightarrow \min \\[1.0em]
        \dot{x}(t) =  \displaystyle (1, 0) + u(t), \quad  u(t) \in \mathrm{R}^2, \quad t \in [0, t_f] \text{ a.e.}, \\[1.0em]
        x(0) = (0, 0) , \quad x_2(t_f) = 1.
    \end{array}
    \right. 
```

The pseudo-Hamiltonian reads

```math
    H(x,p,u) := p_1 + (p | u) - \frac{1}{2} (u_1^2+u_2^2), \quad p^0 = -1.
```

The PMP gives:

```math
\dot{x} = (1,0) + u, \quad \dot{p} = 0, \quad u = p, \quad p(t_f) = (0, \lambda),
```

and so we have to solve the BVP:

```math
        \left\{ 
            \begin{array}{rcl}
                \dot{x}(t)  &=& (1, 0) + p(t),   \\[0.5em]
                \dot{p}(t)  &=& 0,         \\[0.5em]
                x(0) &=& (0,0), \quad x_2(t_f) = 1, \quad p(t_f) = (0, \lambda).
            \end{array}
        \right. 
```

Computing we get,

```math
    u(t) = p(t) = (0, \frac{1}{t_f}), \quad \lambda = \frac{1}{t_f}, \quad x(t) = (t, \frac{t}{t_f}).
```

------

[Part II](lecture2.md)

------
