# Lecture: The indirect simple shooting method - Part II

* Author: Olivier Cots
* Date: March 2021

------

[Part I](lecture1.md)

------

**_Contents_**

[[_TOC_]]

## III) Indirect simple shooting

### a) The shooting equation

>>>

**_Boundary value problem_**

Under our assumptions and thanks to the PMP we have to solve the following boundary value problem with a parameter $\lambda$:

```math
        \left\{ 
            \begin{array}{l}
                \dot{z}(t) = \vec{H}(z(t),u[z(t)]),   \\[0.5em]
                x(0)       = x_0, \quad c(x(t_f)) = 0, \quad p(t_f) = J_c^T(x(t_f)) \lambda,
            \end{array}
        \right.
```

with $`z=(x,p)`$, with $`u[z]`$ the smooth control law in feedback form given by the maximization condition,
and where $`\vec{H}(z, u) := (\nabla_p H(z,u), -\nabla_x H(z,u))`$.
    
>>>

**_Remark._** We can replace $`\dot{z}(t) = \vec{H}(z(t),u[z(t)])`$ by $`\dot{z}(t) = \vec{h}(z(t))`$,
where $`h(z) = H(z, u[z])`$ is the maximized Hamiltonian.

>>>

**_Shooting function_**
 
To solve the BVP, we define a set of nonlinear equations, the so-called the *shooting equations*. To do so, we introduce the *shooting function* $`S \colon \mathrm{R}^n \times \mathrm{R}^k \to \mathrm{R}^k \times \mathrm{R}^n`$:

```math
 S(p_0, \lambda) := 
 \begin{pmatrix}
     c\left( \pi_x( z(t_f, x_0, p_0) ) \right) \\
     \pi_p( z(t_f, x_0, p_0) ) - J_c^T \left( \pi_x( z(t_f, x_0, p_0) ) \right) \lambda
 \end{pmatrix}
```

where $`\pi_x(x,p) := x`$ is the canonical projection into the state space, $`\pi_p(x,p) := p`$ is the canonical projection into the co-state space, and where $`z(t_f, x_0, p_0)`$ is the solution at time $`t_f`$ of 
$`\dot{z}(t) = \vec{H}(z(t), u[z(t)]) = \vec{h}(z(t))`$, $`z(0) = (x_0, p_0)`$.
    
>>>

>>>

**_Indirect simple shooting method_**
 
Solving the BVP is equivalent to find a zero of the shooting function, that is to solve 

```math
    S(p_0, \lambda) = 0.
```

The *indirect simple shooting method* consists in solving this equation.

>>>

In order to solve the shooting equations, we need to compute the control law $`u[\cdot]`$, the Hamiltonian system $`\vec{H}`$ (or $`\vec{h}`$), we need an [integrator method](https://en.wikipedia.org/wiki/Numerical_methods_for_ordinary_differential_equations) to compute the *exponential map* $`\exp(t \vec{H})`$ defined by
```math
\exp(t \vec{H})(x_0, p_0) := z(t, x_0, p_0),
```
and we need a [Newton-like](https://en.wikipedia.org/wiki/Newton%27s_method) solver to solve $`S=0`$.

**_Remark:_** The notation with the exponential mapping is introduced because it is more explicit and permits to show that we need to define the Hamiltonian system and we need to compute the exponential, in order to compute an extremal solution of the PMP.

**_Remark:_**
It is important to understand that if $`(p_0^*, \lambda^*)`$ is solution of $`S=0`$, then the control $`u(\cdot) := u[z(\cdot, x_0, p_0^*)]`$ is a candidate as a solution of the optimal control problem. It is only a candidate and not a solution of the OCP since the PMP gives necessary conditions of optimality. We would have to go further to check  whether the control is locally or globally optimal.

[Additional comments (video)](http://www.youtube.com/watch?v=YVG2Z_TEkBQ)

<div align="center">
    <img src="../images/simple_shooting.png" width="400">
</div>
<div align="center">
<i>
Figure: Illustration of the shooting method in the state-time space. The blue trajectory reaches the target in red. The shooting method consists in finding the right impulse to reach the target.
</i>
</div>

<div align="center">
    <img src="../images/simple_shooting_2.png" width="420">
</div>
<div align="center">
<i>
Figure: Illustration of the shooting method in the cotangent space. The blue extremal reaches the target in red.
</i>
</div>

### b) The iteration of the Newton solver and the Jacobian of the shooting function

[Additional comments (video)](http://www.youtube.com/watch?v=hVbi9kShR90)

### c) A word on the Lagrange multiplier

[Additional comments (video)](http://www.youtube.com/watch?v=3VEi-UHAS6w)

## IV) Numerical resolution of the shooting equations with the nutopy package

This part is there: [notebook lecture](lecture_simple_shooting.ipynb). Please follow the procedure given [here](../../README.md) to install all packages and use `jupyter-lab` to execute the code and do the exercices.
