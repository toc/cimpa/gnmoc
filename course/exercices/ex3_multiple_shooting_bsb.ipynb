{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercice 3: Introduction to indirect multiple shooting: the Bang-Singular-Bang case on a turnpike problem "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Author: Olivier Cots\n",
    "* Date: March 2021\n",
    "\n",
    "------\n",
    "\n",
    "* Back: [Exercice 2](../exercices/ex2_implement_simple_shooting.ipynb) -\n",
    "[Correction](../corrections/ex2_cor_implement_simple_shooting.ipynb)\n",
    "\n",
    "------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## I) Description of the optimal control problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We consider the following optimal control problem:\n",
    "\n",
    "$$ \n",
    "    \\left\\{ \n",
    "    \\begin{array}{l}\n",
    "        \\displaystyle J(u)  := \\displaystyle \\int_0^{t_f} x^2(t) \\, \\mathrm{d}t \\longrightarrow \\min \\\\[1.0em]\n",
    "        \\dot{x}(t) = f(x(t), u(t)) := \\displaystyle u(t), \\quad  |u(t)| \\le 1, \\quad t \\in [0, t_f] \\text{ a.e.},    \\\\[1.0em]\n",
    "        x(0) = 1, \\quad x(t_f) = 1/2.\n",
    "    \\end{array}\n",
    "    \\right. \n",
    "$$\n",
    "\n",
    "To this optimal control problem is associated the stationnary optimization problem\n",
    "\n",
    "$$\n",
    "    \\min_{(x, u)} \\{~ x^2 ~ | ~  (x, u) \\in \\mathrm{R} \\times [-1, 1],~ f(x,u) = u = 0\\}.\n",
    "$$\n",
    "\n",
    "The static solution is thus $(x^*, u^*) = (0, 0)$. This solution may be seen as the static pair $(x, u)$ which minimizes the cost $J(u)$ under\n",
    "the constraint $u \\in [-1, 1]$.\n",
    "It is well known that this problem is what we call a *turnpike* optimal control problem.\n",
    "Hence, if the final time $t_f$ is long enough the solution is of the following form: \n",
    "starting from $x(0)=1$, reach as fast as possible the static solution, stay at the static solution as long as possible before reaching\n",
    "the target $x(t_f)=1/2$. In this case, the optimal control would be\n",
    "\n",
    "$$\n",
    "    u(t) = \\left\\{ \n",
    "    \\begin{array}{lll}\n",
    "        -1            & \\text{if} & t \\in [0, t_1],     \\\\[0.5em]\n",
    "        \\phantom{-}0  & \\text{if} & t \\in (t_1, t_2],   \\\\[0.5em]\n",
    "        +1            & \\text{if} & t \\in (t_2, t_f],\n",
    "    \\end{array}\n",
    "    \\right. \n",
    "$$\n",
    "\n",
    "with $0 < t_1 < t_2 < t_f$. We say that the control is *Bang-Singular-Bang*. A Bang arc corresponds to $u \\in \\{-1, 1\\}$ while a singular control corresponds to $u \\in (-1, 1)$. Since the optimal control law is discontinuous, then to solve this optimal control problem by indirect methods and find the *switching times* $t_1$ and $t_2$, we need to implement what we call a *multiple shooting method*. In the next section we introduce a regularization technique to force the control to be in the set $(-1,1)$ and to be smooth. In this context, we will be able to implement a simple shooting method and determine the structure of the optimal control law. Thanks to the simple shooting method, we will have the structure of the optimal control law together with an approximation of the switching times that we will use as initial guess for the multiple shooting method that we present in the last section.\n",
    "\n",
    "<div class=\"alert alert-warning\">\n",
    "\n",
    "**Main goal**\n",
    "\n",
    "Find the switching times $t_1$ and $t_2$ by multiple shooting.\n",
    "    \n",
    "</div>\n",
    "\n",
    "Steps:\n",
    "\n",
    "1. Regularize the problem and solve the regularized problem by simple shooting.\n",
    "2. Determine the structure of the non-regularized optimal control problem, that is the structure Bang-Singular-Bang, and find a good approximation of the switching times and of the initial co-vector.\n",
    "3. Solve the non-regularized optimal control problem by multiple shooting.\n",
    "\n",
    "**_Remark 1._** See this [page](../lecture/lecture_simple_shooting.ipynb) for a general presentation of the simple shooting method.\n",
    "\n",
    "**_Remark 2._** In this particular example, the singular control does not depend on the costate $p$ since it is constant. This happens in low dimension. This could be taken into consideration to simplify the definition of the multiple shooting method. However, to stay general, we will not consider this particular property in this notebook.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## II) Regularization and simple shooting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We make the following regularization:\n",
    "\n",
    "$$ \n",
    "    \\left\\{ \n",
    "    \\begin{array}{l}\n",
    "        \\displaystyle J(u)  := \\displaystyle \\int_0^{t_f} (x^2(t) - \\varepsilon\\ln(1-u^2(t))) \\, \\mathrm{d}t \\longrightarrow \\min \\\\[1.0em]\n",
    "        \\dot{x}(t) = f(x(t), u(t)) := \\displaystyle u(t), \\quad  |u(t)| \\le 1, \\quad t \\in [0, t_f] \\text{ a.e.},    \\\\[1.0em]\n",
    "        x(0) = 1, \\quad x(t_f) = 1/2.\n",
    "    \\end{array}\n",
    "    \\right. \n",
    "$$\n",
    "\n",
    "Our goal is to determine the structure of the optimal control problem when $(\\varepsilon, t_f) = (0, 2)$. The problem is simpler to solver when $\\varepsilon$ is bigger and $t_f$ is smaller. It is also smooth whenever $\\varepsilon>0$. Hence, we will start by solving the problem for $(\\varepsilon, t_f) = (1, 1)$. In a second step, we will decrease the *penalization term* $\\varepsilon$ and in a final step, we will increase the final time $t_f$ to the final value $2$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preliminaries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import packages\n",
    "import nutopy as nt\n",
    "import nutopy.tools as tools\n",
    "import nutopy.ocp as ocp\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "plt.rcParams['figure.figsize'] = [12, 10]\n",
    "plt.rcParams['figure.dpi'] = 100 # 200 e.g. is really fine, but slower"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Finite differences function for scalar functionnal\n",
    "# Return f'(x).dx\n",
    "def finite_diff(fun, x, dx, *args, **kwargs):\n",
    "    v_eps = np.finfo(float).eps\n",
    "    t = np.sqrt(v_eps) * np.sqrt(np.maximum(1.0, np.abs(x))) / np.sqrt(np.maximum(1.0, np.abs(dx)))\n",
    "    j = (fun(x + t*dx, *args, **kwargs) - fun(x, *args, **kwargs)) / t\n",
    "    return j"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parameters\n",
    "\n",
    "t0        = 0.0\n",
    "x0        = np.array([1.0])\n",
    "xf_target = np.array([0.5])\n",
    "\n",
    "e_init    = 1.0\n",
    "e_final   = 0.002 #\n",
    "\n",
    "tf_init   = 1.0 # With this value the problem is simpler to solver since the trajectory stay \n",
    "                # less time around the static solution\n",
    "tf_final  = 2.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Maximized Hamiltonian and its derivatives"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pseudo-Hamiltonian is (in the normal case)\n",
    "\n",
    "$$\n",
    "    H(x,p,u,\\varepsilon) = pu - x^2 + \\varepsilon ln(1-u^2).\n",
    "$$\n",
    "\n",
    "Note that we put the parameter $\\varepsilon$ into the arguments of the pseudo-Hamiltonian since we will vary it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**_Question 1:_**\n",
    "    \n",
    "Give the maximizing control $u[p, \\varepsilon]$, that is the control in feedback form solution of the maximization condition.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer 1:** To complete here (double-click on the line to complete)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**_Question 2:_**\n",
    "    \n",
    "Complete the code of the maximizing control $u[p, \\varepsilon]$ and its derivative with respect to $p$, that is $\\frac{\\partial u}{\\partial p}[p, \\varepsilon]$.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ----------------------------\n",
    "# Answer 2 to complete here\n",
    "# ----------------------------\n",
    "#\n",
    "# Control in feedback form u[p,e] and its partial derivative wrt. p.\n",
    "#\n",
    "@tools.vectorize(vvars=(1,))\n",
    "def ufun(p, e):\n",
    "    u = 0  ### TO COMPLETE\n",
    "    return u\n",
    "\n",
    "def dufun(p, e):\n",
    "    du = 0  ### TO COMPLETE\n",
    "    return du"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We give next the maximized Hamiltonian with its derivatives. This permits us to define the flow of the associated Hamiltonian vector field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of the maximized Hamiltonian and its derivatives\n",
    "# The second derivative d2hfun is computed by finite differences for a part\n",
    "def dhfun(t, x, dx, p, dp, e):\n",
    "    # dh = dh_x dx + dh_p dp\n",
    "    u  = ufun(p, e)\n",
    "    du = dufun(p, e)\n",
    "    hd = (u+p*du+2.0*e*u*du/(u**2-1.0))*dp - 2.0*x*dx\n",
    "    return hd\n",
    "\n",
    "def d2hfun(t, x, dx, d2x, p, dp, d2p, e):\n",
    "    # d2h = dh_xx dx d2x + dh_xp dp d2x + dh_px dx d2p + dh_pp dp d2p\n",
    "    d2h_xx = -2.0*dx*d2x # dh_xx dx d2x\n",
    "    dh_p   = lambda p: dhfun(t, x, 0.0, p, dp, e) # dh_px = 0 so we can put dx = 0\n",
    "    d2h_pp = finite_diff(dh_p, p, d2p) # dh_pp dp d2p\n",
    "    hdd    = d2h_xx + d2h_pp\n",
    "    return hdd\n",
    "\n",
    "@tools.tensorize(dhfun, d2hfun, tvars=(2, 3))\n",
    "def hfun(t, x, p, e):\n",
    "    u = ufun(p, e)\n",
    "    h = p*u - x**2 + e*(np.log(1.0-u**2))\n",
    "    return h\n",
    "\n",
    "h = ocp.Hamiltonian(hfun)   # The Hamiltonian object\n",
    "\n",
    "f = ocp.Flow(h)             # The flow associated to the Hamiltonian object is \n",
    "                            # the exponential mapping with its derivative\n",
    "                            # that can be used to define the Jacobian of the \n",
    "                            # shooting function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shooting function and its derivative"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The shooting function is\n",
    "\n",
    "$$\n",
    "    S(p_0, \\varepsilon, t_f) = \\pi_x(z(t_f, 1, p_0, \\varepsilon)) - 1/2,\n",
    "$$\n",
    "\n",
    "where $z(t_f, x_0, p_0, \\varepsilon)$ is the solution of the associated Hamiltonian system \n",
    "with the initial condition $z(0) = (x_0, p_0)$. Note that the Hamiltonian system depends on $\\varepsilon$. We put $\\varepsilon$ and $t_f$ into \n",
    "the arguments of the shooting function since we will vary them.\n",
    "\n",
    "<div class=\"alert alert-warning\">\n",
    "\n",
    "**Procedure**\n",
    "\n",
    "First solve $S=0$ for $(\\varepsilon, t_f) = (1,1)$ then decrease $\\varepsilon$ to $0.002$, and finish by increasing $t_f$ to 2.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**_Question 3:_**\n",
    "    \n",
    "Complete the code of the shooting function.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ----------------------------\n",
    "# Answer 3 to complete here\n",
    "# ----------------------------\n",
    "#\n",
    "# Definition of the shooting function and its partial derivative wrt. p0 against the vector dp0\n",
    "#\n",
    "def shoot(p0, e, tf):\n",
    "    s = 0  ### TO COMPLETE: use the flow f, the parameters t0, x0 and xf_target\n",
    "    return s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dshoot(p0, dp0, e, tf):\n",
    "    (xf, dxf), _ = f(t0, x0, (p0, dp0), tf, e)\n",
    "    ds = dxf\n",
    "    return ds\n",
    "\n",
    "shoot = nt.tools.tensorize(dshoot, tvars=(1,))(shoot)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function to plot the solution\n",
    "def plotSolution(p0, e, tf):\n",
    "\n",
    "    N      = 200\n",
    "    tspan  = list(np.linspace(t0, tf, N+1))\n",
    "    xf, pf = f(t0, x0, p0, tspan, e)\n",
    "    u      = ufun(pf, e)\n",
    "\n",
    "    fig = plt.figure(); plt.tight_layout()\n",
    "    ax  = fig.add_subplot(311); ax.plot(tspan, xf); ax.set_xlabel('t'); ax.set_ylabel('$x$'); ax.axhline(0, color='k')\n",
    "    ax  = fig.add_subplot(312); ax.plot(tspan, pf); ax.set_xlabel('t'); ax.set_ylabel('$p$'); ax.axhline(0, color='k')\n",
    "    ax  = fig.add_subplot(313); ax.plot(tspan,  u); ax.set_xlabel('t'); ax.set_ylabel('$u$'); ax.axhline(0, color='k')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Resolution of the regularized problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Shooting for (tf, e) = (tf_init, e_init)\n",
    "p0_guess = np.array([0.1])\n",
    "nlefun   = lambda p0: shoot(p0, e_init, tf_init)\n",
    "sol_nle  = nt.nle.solve(nlefun, p0_guess, df=nlefun)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot solution for (tf, e) = (tf_init, e_init)\n",
    "plotSolution(sol_nle.x, e_init, tf_init)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of the homotopic function and its first order derivative\n",
    "# This function is used to solve S=0 for different values of e=epsilon and tf.\n",
    "def dhomfun(p0, dp0, e, de, tf, dtf):\n",
    "    #\n",
    "    (xf, dxf), (pf, dpf) = f(t0, x0, (p0, dp0), tf, e)    \n",
    "    #\n",
    "    s     = xf - xf_target\n",
    "    #\n",
    "    ds_p0 = dxf\n",
    "    ds_tf = ufun(pf, e) * dtf # dS_tf dtf = u dtf\n",
    "    #\n",
    "    fun = lambda e: float(f(t0, x0, p0, tf, e)[0])\n",
    "    ds_e = finite_diff(fun, e, de) # dS_e de\n",
    "    #\n",
    "    ds    = ds_p0 + ds_e + ds_tf\n",
    "    return s, ds\n",
    "\n",
    "@tools.tensorize(dhomfun, tvars=(1, 2, 3), full=True)\n",
    "def homfun(p0, e, tf):\n",
    "    xf, pf = f(t0, x0, p0, tf, e)  # We use the flow to get z(tf, x0, p0, e)\n",
    "    s = xf - xf_target             # x(tf, x0, p0) - xf_target\n",
    "    return s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Making the penalization smaller: homotopy on e\n",
    "p0         = sol_nle.x\n",
    "sol_path_e = nt.path.solve(homfun, p0, e_init, e_final, args=tf_init, df=homfun)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot solution for (tf, e) = (tf_init, e_final)\n",
    "plotSolution(sol_path_e.xf, e_final, tf_init)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Making the time bigger: homotopy on tf\n",
    "p0          = sol_path_e.xf                       # sol is coming from last homotopy\n",
    "pathfun     = lambda p0, tf, e: homfun(p0, e, tf) # invert order of arguments\n",
    "sol_path_tf = nt.path.solve(pathfun, p0, tf_init, tf_final, args=e_final, df=pathfun)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot solution for (tf, e) = (tf_final, e_final)\n",
    "plotSolution(sol_path_tf.xf, e_final, tf_final)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## III) Resolution of the optimal control problem by multiple shooting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We come back to the original optimal control problem:\n",
    "\n",
    "$$ \n",
    "    \\left\\{ \n",
    "    \\begin{array}{l}\n",
    "        \\displaystyle J(u)  := \\displaystyle \\int_0^{t_f} x^2(t) \\, \\mathrm{d}t \\longrightarrow \\min \\\\[1.0em]\n",
    "        \\dot{x}(t) = f(x(t), u(t)) := \\displaystyle u(t), \\quad  |u(t)| \\le 1, \\quad t \\in [0, t_f] \\text{ a.e.},    \\\\[1.0em]\n",
    "        x(0) = 1, \\quad x(t_f) = 1/2.\n",
    "    \\end{array}\n",
    "    \\right. \n",
    "$$\n",
    "\n",
    "We have determined that the optimal control follows the strategy:\n",
    "\n",
    "$$\n",
    "    u(t) = \\left\\{ \n",
    "    \\begin{array}{lll}\n",
    "        -1            & \\text{if} & t \\in [0, t_1],     \\\\[0.5em]\n",
    "        \\phantom{-}0  & \\text{if} & t \\in (t_1, t_2],   \\\\[0.5em]\n",
    "        +1            & \\text{if} & t \\in (t_2, t_f],\n",
    "    \\end{array}\n",
    "    \\right. \n",
    "$$\n",
    "\n",
    "with $0 < t_1 < t_2 < t_f=2$. \n",
    "\n",
    "\n",
    "<div class=\"alert alert-warning\">\n",
    "\n",
    "**Goal**\n",
    "\n",
    "The goal is to find the values of the switching times $t_1$ and $t_2$ together with the initial covector $p_0$ (see Remark 2).\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Maximized Hamiltonian and its derivatives"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define first the three control laws $u \\equiv \\{-1, 0, 1\\}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Controls in feedback form\n",
    "@tools.vectorize(vvars=(1, 2, 3))\n",
    "def uplus(t, x, p):\n",
    "    u = +1.0\n",
    "    return u\n",
    "\n",
    "@tools.vectorize(vvars=(1, 2, 3))\n",
    "def uminus(t, x, p):\n",
    "    u = -1.0\n",
    "    return u\n",
    "\n",
    "@tools.vectorize(vvars=(1, 2, 3))\n",
    "def using(t, x, p):\n",
    "    u = 0.0\n",
    "    return u"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pseudo-Hamiltonian is\n",
    "\n",
    "$$\n",
    "    H(x,p,u) = pu - x^2.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**_Question 4:_**\n",
    "    \n",
    "Complete the code of the Hamiltonian for $u \\equiv +1$, with its derivatives.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ----------------------------\n",
    "# Answer 4 to complete here\n",
    "# ----------------------------\n",
    "#\n",
    "# Definition of the Hamiltonian and its derivatives for u = 1\n",
    "#\n",
    "def dhfunplus(t, x, dx, p, dp):\n",
    "    # dh = dh_x dx + dh_p dp\n",
    "    hd = 0 ### TO COMPLETE: use uplus\n",
    "    return hd\n",
    "    \n",
    "def d2hfunplus(t, x, dx, d2x, p, dp, d2p):\n",
    "    # d2h = dh_xx dx d2x + dh_xp dp d2x + dh_px dx d2p + dh_pp dp d2p\n",
    "    hdd = 0 ### TO COMPLETE\n",
    "    return hdd\n",
    "\n",
    "@tools.tensorize(dhfunplus, d2hfunplus, tvars=(2, 3))\n",
    "def hfunplus(t, x, p):\n",
    "    h = 0 ### TO COMPLETE: use uplus\n",
    "    return h\n",
    "\n",
    "hplus = ocp.Hamiltonian(hfunplus)\n",
    "fplus = ocp.Flow(hplus)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We give the Hamiltonians for $u=-1$ and $u=0$ with their derivatives."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of the Hamiltonian and its derivatives for u = -1\n",
    "def dhfunminus(t, x, dx, p, dp):\n",
    "    # dh = dh_x dx + dh_p dp\n",
    "    u  = uminus(t, x, p)\n",
    "    hd = u*dp - 2.0*x*dx\n",
    "    return hd\n",
    "    \n",
    "def d2hfunminus(t, x, dx, d2x, p, dp, d2p):\n",
    "    # d2h = dh_xx dx d2x + dh_xp dp d2x + dh_px dx d2p + dh_pp dp d2p\n",
    "    hdd    = -2.0 * d2x * dx\n",
    "    return hdd\n",
    "\n",
    "@tools.tensorize(dhfunminus, d2hfunminus, tvars=(2, 3))\n",
    "def hfunminus(t, x, p):\n",
    "    u = uminus(t, x, p)\n",
    "    h = p*u - x**2\n",
    "    return h\n",
    "\n",
    "hminus = ocp.Hamiltonian(hfunminus)\n",
    "fminus = ocp.Flow(hminus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of the Hamiltonian and its derivatives for u = 0\n",
    "def dhfunsing(t, x, dx, p, dp):\n",
    "    # dh = dh_x dx + dh_p dp\n",
    "    u  = using(t, x, p)\n",
    "    hd = u*dp - 2.0*x*dx\n",
    "    return hd\n",
    "    \n",
    "def d2hfunsing(t, x, dx, d2x, p, dp, d2p):\n",
    "    # d2h = dh_xx dx d2x + dh_xp dp d2x + dh_px dx d2p + dh_pp dp d2p\n",
    "    hdd    = -2.0 * d2x * dx\n",
    "    return hdd\n",
    "\n",
    "@tools.tensorize(dhfunsing, d2hfunsing, tvars=(2, 3))\n",
    "def hfunsing(t, x, p):\n",
    "    u = using(t, x, p)\n",
    "    h = p*u - x**2\n",
    "    return h\n",
    "\n",
    "hsing = ocp.Hamiltonian(hfunsing)\n",
    "fsing = ocp.Flow(hsing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shooting function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The multiple shooting function is given by\n",
    "\n",
    "$$\n",
    " S(p_0, t_1, t_2) := \n",
    " \\begin{pmatrix}\n",
    "     x(t_1, t_0, x_0, p_0, u_-) \\\\\n",
    "     p(t_1, t_0, x_0, p_0, u_-) \\\\\n",
    "     x(t_f, t_2, x_2, p_2, u_+) - 1/2\n",
    " \\end{pmatrix},\n",
    "$$\n",
    "\n",
    "where $z_2 := (x_2, p_2) = z(t_2, t_1, x_1, p_1, u_0)$, $z_1 := (x_1, p_1) = z(t_1, t_0, x_0, p_0, u_-)$ and where z(t, s, a, b, u) is the solution at time $t$ of the Hamiltonian system associated to the control u starting at time $s$ at the initial condition $z(s) = (a,b)$.\n",
    "\n",
    "We have introduced the notation $u_-$ for $u\\equiv -1$, $u_0$ for $u\\equiv 0$ and $u_+$ for $u\\equiv +1$.\n",
    "\n",
    "**_Remark:_** We know that $(x_2, p_2)=(0,0)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**_Question 5:_**\n",
    "    \n",
    "Complete the code of the multiple shooting function.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ----------------------------\n",
    "# Answer 5 to complete here\n",
    "# ----------------------------\n",
    "#\n",
    "# Multiple shooting function\n",
    "#\n",
    "\n",
    "tf = tf_final # we set the final time to the value tf_final\n",
    "\n",
    "def shoot_multiple(y):\n",
    "    p0 = y[0]\n",
    "    t1 = y[1]\n",
    "    t2 = y[2]\n",
    "    \n",
    "    s = np.zeros([3]) ### TO COMPLETE: use fminus, fsin, fplus, t0, t1, t2, tf, x0, xf_target\n",
    "    \n",
    "    return s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Resolution of the shooting function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**_Question 6:_**\n",
    "    \n",
    "Give initial guesses for the times $t_1$ and $t_2$ according to the solution of the regularized problem.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ----------------------------\n",
    "# Answer 6 to complete here\n",
    "# ----------------------------\n",
    "#\n",
    "# Initial guess for the Newton solver\n",
    "\n",
    "t1_guess = 0.0 # to update\n",
    "t2_guess = 0.0 # to update\n",
    "\n",
    "p0_guess = sol_path_tf.xf # from previous homotopy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Resolution of the shooting function\n",
    "y_guess  = np.array([float(p0_guess), t1_guess, t2_guess])\n",
    "sol_nle_mul  = nt.nle.solve(shoot_multiple, y_guess)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# function to plot solution\n",
    "\n",
    "def plotSolutionBSB(p0, t1, t2, tf):\n",
    "\n",
    "    N      = 20\n",
    "    \n",
    "    tspan1  = list(np.linspace(t0, t1, N+1))\n",
    "    tspan2  = list(np.linspace(t1, t2, N+1))\n",
    "    tspanf  = list(np.linspace(t2, tf, N+1))\n",
    "        \n",
    "    x1, p1 = fminus(t0, x0, p0, tspan1)  # on [ 0, t1]\n",
    "    x2, p2 =  fsing(t1, x1[-1], p1[-1], tspan2)  # on [t1, t2]\n",
    "    xf, pf =  fplus(t2, x2[-1], p2[-1], tspanf)  # on [t2, tf]\n",
    "    \n",
    "    u1     = uminus(tspan1, x1, p1)\n",
    "    u2     =  using(tspan2, x2, p2)\n",
    "    uf     =  uplus(tspanf, xf, pf)\n",
    "\n",
    "    fig = plt.figure(); plt.tight_layout()\n",
    "    ax  = fig.add_subplot(311); ax.plot(tspan1, x1); ax.plot(tspan2, x2); ax.plot(tspanf, xf); \n",
    "    ax.set_xlabel('t'); ax.set_ylabel('$x$'); ax.axhline(0, color='k')\n",
    "    ax  = fig.add_subplot(312);  ax.plot(tspan1, p1); ax.plot(tspan2, p2); ax.plot(tspanf, pf);\n",
    "    ax.set_xlabel('t'); ax.set_ylabel('$p$'); ax.axhline(0, color='k')\n",
    "    ax  = fig.add_subplot(313);  ax.plot(tspan1, u1); ax.plot(tspan2, u2); ax.plot(tspanf, uf);\n",
    "    ax.set_xlabel('t'); ax.set_ylabel('$u$'); ax.axhline(0, color='k')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot solution\n",
    "p0 = sol_nle_mul.x[0]\n",
    "t1 = sol_nle_mul.x[1]\n",
    "t2 = sol_nle_mul.x[2]\n",
    "plotSolutionBSB(p0, t1, t2, tf)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
